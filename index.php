<?php
  require_once('animal.php');
  require_once ('frog.php');
  require_once ('ape.php');

  $sheep = new Animal("shaun");
  echo "Nama : " . $sheep->name . "<br>"; // "shaun"
  echo "Legs : " . $sheep->legs . "<br>"; // 4
  echo "Cool Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"


  $kodok = new Frog("buduk");
  echo "Nama : " . $kodok->name . "<br>";
  echo "Legs : " . $kodok->legs . "<br>"; // 4
  echo "Cool Blooded : " . $kodok->cold_blooded . "<br>";
  echo "Jump : " ;
  echo  $kodok->jump() . "<br><br>";

  $sungokong = new Ape("kera sakti");
  echo "Nama : " . $sungokong->name . "<br>";
  echo "Legs : " . $sungokong->legs . "<br>"; // 4
  echo "Cool Blooded : " . $sungokong->cold_blooded . "<br>";
  echo "Yell : " ;
  echo $sungokong->yell() . "<br><br>";


 ?>
